import { CACHE } from '../utils/cache.js';
export const TokenStorage = {
  setData: (accessToken) => {
    CACHE.accessToken = accessToken
    wx.setStorageSync('accessToken', accessToken)
  },
  getData: () => {
    if (CACHE.accessToken) {
      return CACHE.accessToken
    }
    return wx.getStorageSync("accessToken")
  },
  clearData: ()=> {
    wx.removeStorageSync('accessToken')
  }
}

export const UserStorage = {
  setData: function (obj) {
    CACHE.userInfo = obj
    wx.setStorage({
      key: "userInfo",
      data: obj
    })
  },
  getData: function () {
    if (CACHE.userInfo) {
      return CACHE.userInfo
    }
    return wx.getStorageSync("userInfo")
  }
}

export const WeRunStorage ={
  setData: function (weRunLastTime){
    CACHE.weRunLastTime = weRunLastTime
    wx.setStorageSync("weRunLastTime", weRunLastTime);
  },
  getData: function(){
    if (CACHE.weRunLastTime) {
      return CACHE.weRunLastTime
    }
    return wx.getStorageSync("weRunLastTime");
  }
}
